@Carrito
Feature: Carrito

@C.1
Scenario: Agregar producto al carrito
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado

@C.2
Scenario: Quitar producto al carrito
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario quita las unidades del producto
	Then El producto fue retirado del carrito

@C.3
Scenario: Vaciar Carrito
	Given El usuario se loguea en la app
	Then Inicia sesion correctamente
	When El usuario chequea el saldo disponible
	Then El usuario ve su saldo disponible
	When Se escanea el QR del producto seleccionado
	And Se agrega producto al carrito
	Then Articulo Cargado
	When El usuario vacia el carrito
	Then Carrito Vacio

