﻿using BaseFramework.Lib.Api.Models;
using Newtonsoft.Json;


namespace BaseFramework.Environment;

public class EnvironmentManager : Environment
{
    private readonly StreamReader _reader;


    public EnvironmentManager(string environment)
    {
        try
        {
            var filePath = $"../../../Environment/Json/{environment}.json";
            _reader = new StreamReader(filePath);
        }
        catch (FileNotFoundException)
        {
            throw new FileNotFoundException($"Environment named {environment} not found.");
        }

    }

    public void SetEnvironmentVariables()
    {
        dynamic environmentVariables = JsonConvert.DeserializeObject(_reader.ReadToEnd())!;

        try
        {
            Host = environmentVariables.host;
            User = environmentVariables.user.ToObject<User>();
        }
        catch (NullReferenceException)
        {
            throw new NullReferenceException("Environment JSON Object could not be deserialized");
        }
    }
}