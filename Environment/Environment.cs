﻿using Newtonsoft.Json;

namespace BaseFramework.Environment;

public class Environment
{
    [JsonProperty(PropertyName = "host")]
    public string Host { get; set; }

    public User User { get; set; }

    public Environment()
    {
        Host = string.Empty;
        User = new User();
    }

    public Environment( string host)
    {
        Host = host;
        User = new User();
    }

    public void CleanEnvironment()
    {
        this.Host = string.Empty;
        User = new User();
    }
}