﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Environment
{
    public class User
    {
        [JsonProperty(PropertyName = "nombre")]
        public string Nombre { get; set; }

        [JsonProperty(PropertyName = "userIncorrecto")]
        public string UserInc { get; set; }

        [JsonProperty(PropertyName = "newPass")]
        public string NewPass { get; set; }

        [JsonProperty(PropertyName = "passErroneo")]
        public string PassErroneo { get; set; }

        [JsonProperty(PropertyName = "pass")]
        public string Pass { get; set; }

        [JsonProperty(PropertyName = "mail")]
        public string Mail { get; set; }

        [JsonProperty(PropertyName = "idUsuario")]
        public string idUsuario { get; set; }

        [JsonProperty(PropertyName = "userToken")]
        public string userToken { get; set; }

        [JsonProperty(PropertyName = "passToken")]
        public string passToken { get; set; }

    }
}
