﻿using BaseFramework.Environment;
using BaseFramework.Lib.Api;

namespace BaseFramework.Hooks;

[Binding]
public class Hooks
{

    private static EnvironmentManager? _envManager;
    private readonly ScenarioContext _scenarioContext;

    public Hooks(ScenarioContext scenarioContext)
    {

        _scenarioContext = scenarioContext;
        _envManager = new EnvironmentManager(System.Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "dev");
        //_envManager = new EnvironmentManager(System.Environment.GetEnvironmentVariable("ENVIRONMENT") ?? "tst");

    }


    [BeforeScenario()]
    public void BeforeScenario()
    {
        _envManager?.CleanEnvironment();
        _envManager?.SetEnvironmentVariables();
        if (_scenarioContext.ContainsKey("host")) return;
        _scenarioContext.Add("host", _envManager?.Host);
        _scenarioContext.Add("user", _envManager);

    }

    [AfterScenario]
    public void AfterScenario()
    {
        _scenarioContext.Clear();
    }
}