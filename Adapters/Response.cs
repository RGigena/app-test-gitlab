﻿namespace BaseFramework.Adapters;

public class Response
{
    public string Content { get; set; } = "";
    public string ErrorMessage { get; set; } = "";
    public int StatusCode { get; set; } = 0;
    public bool IsSuccessful { get; set; } = false;
    public string ResponseUri { get; set; } = "";
}