﻿using RestSharp;

namespace BaseFramework.Adapters;

public class ResponseAdapter
{
    public Response ToResponse(RestResponse restResponse) => new Response
    {
        Content = restResponse.Content ?? "",
        ErrorMessage = restResponse.ErrorMessage ?? "",
        StatusCode = (int)restResponse.StatusCode,
        IsSuccessful = restResponse.IsSuccessful,
        ResponseUri = restResponse.ResponseUri?.ToString()!,
    };
}