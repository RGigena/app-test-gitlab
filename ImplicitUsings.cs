﻿global using TechTalk.SpecFlow;
global using BaseFramework.Adapters;
global using BaseFramework.Environment;
global using BaseFramework.Lib.Api;
global using BaseFramework.Lib.Api.Models;
global using Newtonsoft.Json;
global using NUnit.Framework;
global using System.Net.Http.Headers;
