﻿using BaseFramework.Adapters;
using BaseFramework.Lib.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Mock
{
    public static class CartMock
    {

        public static async Task<Response> quitarPermisos(int IdConsumidor, int IdProducto, int IdTienda)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = IdConsumidor, idProducto = IdProducto, idTienda = IdTienda };
            var responseObject = new { status = 1, message = "Permisos de consumo actualizados", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> obtenerUsuarioId(string user, string mail)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = 1};
            var responseObject = new { status = 1, message = (object)null, body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> GetItemSinPermisos(string idProducto)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var responseObject = new { status = 0, message = "NO tienes permisos para adquirir este producto", body = (object)null};
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> quitarPermisosTienda(int IdConsumidor, int IdTienda)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = IdConsumidor, idTienda = IdTienda };
            var responseObject = new { status = 1, message = "Permisos de consumo en la tienda actualizados", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> quitarPermisosCarga(int IdConsumidor)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = IdConsumidor};
            var responseObject = new { status = 1, message = "Permisos de carga actualizados", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> quitarSaldo(int IdConsumidor, int monto)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = IdConsumidor, monto = monto };
            var responseObject = new { status = 1, message = "Saldo Actualizado", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> compraFallida()
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var responseObject = new { status = 1, message = "No posee el saldo suficiente para realizar esta compra", body = (object)null};
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> GetItem()
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            response.Content = "{ 'status': 0, 'message': null, 'body': null}";
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> GetItemLimitado()
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            response.Content = "{ 'status': 1, 'message': 'El usuario superó el limite de consumos de este producto', 'body': null}";
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> cargarSaldo()
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var responseObject = new { status = 1, message = "No está habilitado para la carga de crédito electrónica.", body = (object)null };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> agregarPLA(int unidades)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { unidades = unidades};
            var responseObject = new { status = 1, message = "EL usuario no puede consumir esa cantidad de unidades del producto", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> noPuede()
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var responseObject = new { status = 1, message = "El usuario no puede comprar en dos tiendas distintas a la vez.", body = (object)null};
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> limitarProductos(int IdConsumidor, int idProducto, int cantidadUnidades)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = IdConsumidor, idProducto = idProducto, cantidad = cantidadUnidades };
            var responseObject = new { status = 1, message = "Se limitó la cantidad de unidades a consumir", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> suspenderCuenta(int IdConsumidor)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var responseObject = new { status = 1, message = "Esta cuenta esta suspendida", body = (object)null};
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> tiendasHabilitadas(int IdConsumidor)
        {
            await Task.Delay(1500);


            TiendaModel tienda1 = new TiendaModel { Id = 1, Name = "Tienda Corporativa Claro"};
            TiendaModel tienda2 = new TiendaModel { Id = 2, Name = "Tienda Corporativa Auto City"};

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { tienda = new List<TiendaModel> { tienda1, tienda2 }};
            var responseObject = new { status = 0, message = "Tiendas Habilitadas", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> tiendasNOHabilitadas(int IdConsumidor)
        {
            await Task.Delay(1500);


            TiendaModel tienda1 = new TiendaModel { Id = 1, Name = "Tienda Corporativa Personal Flow" };
            TiendaModel tienda2 = new TiendaModel { Id = 2, Name = "Tienda Corporativa Farmacity" };

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { tienda = new List<TiendaModel> { tienda1, tienda2 } };
            var responseObject = new { status = 0, message = "Tiendas No Habilitadas", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> miConsumidorID(string name)
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            var body = new { idConsumidor = 1};
            var responseObject = new { status = 1, message = "Permisos de consumo actualizados", body };
            response.Content = JsonConvert.SerializeObject(responseObject);
            response.StatusCode = 200;
            return response;
        }

        public static async Task<Response> GetTienda()
        {
            await Task.Delay(1500);

            Response response = new Response();
            response.IsSuccessful = true;
            response.Content = "{ 'status': 1, 'message': 'NO tienes permisos para consumir en esta tienda.', 'body': null}";
            response.StatusCode = 200;
            return response;
        }
    }
}
