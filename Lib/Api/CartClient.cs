﻿using BaseFramework.Adapters;
using BaseFramework.Lib.Api.Mock;
using BaseFramework.Lib.Api.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api
{
    internal class CartClient : BaseClient
    {
        public CartClient(string baseUrl, string token = "") : base(baseUrl, token)
        {
        }

        //Elimina todos los item del carrito
        public Response Eliminar()
        {
            try
            {
                string endpoint = "/WCS/api/cart/delete";
                Response response = Put(endpoint);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Elimina un item con el itemID
        public Response EliminarItem(string itemId)
        {
            try
            {
                string endpoint = $"/WCS/api/cart/item/delete/{itemId}";
                Response response = Put(endpoint);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Obtiene los datos del producto a traves del QR
        //Producto tienda corporativa
        public Response obtenerQr(string qr)
        {
            try
            {
                string endpoint = $"/WCS/api/qr?value={qr}";
                Response response = Get(endpoint, new {qr});
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Agrega los productos al carrito
        public Response agregarProducto(string IdVendingExpendedoraSeleccion, string Count)
        {
            try
            {
                string endpoint = "/WCS/api/cart/item";
                Response response = Post(endpoint, new { IdVendingExpendedoraSeleccion, Count });
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Pagar los productos
        public Response pagarProducto()
        {
            string endpoint = "/WCS/api/cart/checkout";
            Response response = Post(endpoint);
            return response;
        }

        //Obtiene el carrito activo
        public Response carritoActivo()
        {
            try
            {
                string endpoint = "/WCS/api/cart/items";
                Response response = Get(endpoint);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //MOCK
        //TODO: Reemplazarlos cuando este lista la API
        public async Task<Response> quitarPermisos(int IdConsumidor, int IdProducto, int idTienda)
        {
            try
            {
                Response response = await CartMock.quitarPermisos(IdConsumidor, IdProducto, idTienda);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> saberId(string userName, string mail)
        {
            try
            {
                Response response = await CartMock.obtenerUsuarioId(userName, mail);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> obtenerProducto(string IdProducto)
        {
            try
            {
                Response response = await CartMock.GetItemSinPermisos(IdProducto);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> quitarPermisosTienda(int IdConsumidor, int IdTienda)
        {
            try
            {
                Response response = await CartMock.quitarPermisosTienda(IdConsumidor, IdTienda);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> quitarCargaElectronica(int idConsumidor)
        {
            try
            {
                Response response = await CartMock.quitarPermisosCarga(idConsumidor);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> cargaCredito()
        {
            try
            {
                Response response = await CartMock.cargarSaldo();
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> agregarProductoLimitadoA(int unidades)
        {
            try
            {
                Response response = await CartMock.agregarPLA(unidades);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> usuarioNoPuedeComprar()
        {
            try
            {
                Response response = await CartMock.noPuede();
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> quitarSaldo(int idConsumidor, int Monto)
        {
            try
            {
                Response response = await CartMock.quitarSaldo(idConsumidor, Monto);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> compraFallida()
        {
            try
            {
                Response response = await CartMock.compraFallida();
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> limitarProducto(int idConsumidor, int idProducto, int cantidadUnidades)
        {
            try
            {
                Response response = await CartMock.limitarProductos(idConsumidor, idProducto, cantidadUnidades);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Response> suspenderUsuario(int idConsumidor)
        {
            try
            {
                Response response = await CartMock.suspenderCuenta(idConsumidor);
                return response;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
