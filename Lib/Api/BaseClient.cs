﻿using BaseFramework.Adapters;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers.NewtonsoftJson;

namespace BaseFramework.Lib.Api;

public class BaseClient
{
    private readonly ResponseAdapter _adapter;
    private RestRequest? _request;
    private static RestClient? _client;
    //private string BaseUrl { get; set; 
    protected BaseClient(string baseUrl, string token = "")
    {
        _adapter = new ResponseAdapter();
        //BaseUrl = baseUrl;

        _client = new RestClient(baseUrl, configureSerialization: s => s.UseNewtonsoftJson(
    settings: new JsonSerializerSettings
    {
        // Configure your JsonSerializerSettings here
        Formatting = Formatting.Indented, // Example setting
        NullValueHandling = NullValueHandling.Ignore, // Another example setting
                                                      // Add any other settings you need
        })
        );

        if (token != "" || token != null)
        {
            _client.AddDefaultHeader("Authorization", "Bearer " + token);
        }
    }

    protected Response Get(string endpoint)
    {
        _request = new RestRequest(endpoint);
        return _adapter.ToResponse(_client!.Get(_request));
    }

    protected Response Get(string endpoint, object payload)
    {
        _request = new RestRequest(endpoint).AddBody(payload);
        return _adapter.ToResponse(_client!.Get(_request));
    }

    protected Response Post(string endpoint, object payload)
    {
        _request = new RestRequest(endpoint)
            .AddHeader("Content-Type", "application/json")
            .AddBody(payload);
        var response = _adapter.ToResponse(_client!.Post(_request));
        return response;
    }

    protected Response Post(string endpoint)
    {
        _request = new RestRequest(endpoint);
        var response = _adapter.ToResponse(_client!.Post(_request));
        return response;
    }

    protected Response Put(string endpoint, object payload)
    {
        _request = new RestRequest(endpoint)
            .AddBody(payload);
        return _adapter.ToResponse(_client!.Put(_request));
    }

    protected Response Put(string endpoint)
    {
        _request = new RestRequest(endpoint);
        return _adapter.ToResponse(_client!.Put(_request));
    }

    protected Response Delete(string endpoint)
    {
        _request = new RestRequest(endpoint);
        return _adapter.ToResponse(_client!.Delete(_request));
    }

    public void Dispose()
    {
        _client?.Dispose();
        GC.SuppressFinalize(this);
    }
}