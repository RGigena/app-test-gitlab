﻿namespace BaseFramework.Lib.Api
{
    public class AuthorizationAPI
    {
        private HttpClient _httpClient;
        public AuthorizationAPI()
        {}

        public async Task<string> GetAccessTokenAsync(string username, string password)
        {
            string apiUrl = "http://192.168.10.111/QA/VC/DataApi/api/v1/auth/token"; //host + "api/v1/auth/token";
            
            // Configurar el cliente HTTP y deshabilitar la validación de certificados
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
            HttpClient client = new HttpClient(handler);
            // Crear un objeto JSON con los datos de autenticación
            var authenticationData = new
            {
                username,
                password
            };
            // Convertir el objeto en una cadena JSON
            var jsonContent = Newtonsoft.Json.JsonConvert.SerializeObject(authenticationData);
            // Crear el contenido de la solicitud HTTP
            var content = new StringContent(jsonContent);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            // Realizar la solicitud POST a la API
            HttpResponseMessage response = await client.PostAsync(apiUrl, content);
            if (response.IsSuccessStatusCode)
            {
                // Leer la respuesta JSON
                var responseContent = await response.Content.ReadAsStringAsync();
                // Deserializar la respuesta JSON en un objeto AuthorizationResponse
                AuthorizationModel authorizationResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthorizationModel>(responseContent);
                // Obtener el token de acceso del objeto AuthorizationResponse
                string accessToken = authorizationResponse.token;
                // Cerrar el cliente HTTP
                client.Dispose();
                return accessToken;
            }
            else
            {
                Console.WriteLine("La solicitud no se completó con éxito. Código de estado: " + response.StatusCode);
                string accessToken = "";
                // Cerrar el cliente HTTP
                client.Dispose();
                return accessToken;
            }
        }
    }
}
