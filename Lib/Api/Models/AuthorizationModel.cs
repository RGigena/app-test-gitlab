﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class AuthorizationModel
    {
        public string token { get; set; }
        public DateTime expiration { get; set; }
    }
}
