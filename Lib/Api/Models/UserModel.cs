﻿using Newtonsoft.Json;
using RestSharp;

namespace BaseFramework.Lib.Api.Models;

public class UserModel
{
    [JsonProperty(PropertyName = "body")]
    public BodyModel Body { get; set; }

    [JsonProperty(PropertyName = "status")]
    public string Status { get; set; }

    [JsonProperty(PropertyName = "message")]
    public string Message { get; set; }

    public string User { get; set; }
    public string idUsuario { get; set; }
    public string UserName { get; set; }
    public string UserPass { get; set; }
    public string newUserPass { get; set; }
    public string userEmail { get; set; }


    public UserModel()
    {
        Body = new BodyModel();
        Status = "0";
        Message = null;
        UserPass = string.Empty;
        UserName = string.Empty;
    }
}