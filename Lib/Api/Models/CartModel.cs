﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class CartModel
    {
        public string? unidades { get; set; }
        public string? status { get; set; }
        public string? message { get; set; }

        [JsonProperty(PropertyName = "body")]
        public BodyModel Body { get; set; }
    }
}
