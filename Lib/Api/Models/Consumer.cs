﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BaseFramework.Lib.Api.Models
{
    public class Consumer
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string RecoveryCode { get; set; }
        public Consumer(string id, string email, string username, string password, string recoveryCode)
        {
            Id = id;
            Email = email;
            Username = username;
            Password = password;
            RecoveryCode = recoveryCode;
        }
        public Consumer() { }
    }
}
