﻿using BaseFramework.Adapters;
using System.Net;
using System.Net.Http;
using System.Text;

namespace BaseFramework.Lib.Api;

public class LoginClient : BaseClient
{
    public LoginClient(string baseUrl, string token = "") : base(baseUrl, token)
    {
    }

    //Iniciar Sesion
    public Response Login(string user, string password)
    {
        try
        {
            string endpoint = "/WCS/api/session/login";
            Response response = Post(endpoint, new { user, password });
            return response;
        }
        catch(Exception ex) {
            throw ex;
        }
    }

    //Cambiar la contraseña una vez iniciada la sesion.
    public Response cambiarContrasenia(string Email, string NewPass)
    {
        try
        {
            string endpoint = "/WCS/api/perfil/pass";
            Response response = Post(endpoint, new { Email, NewPass });
            return response;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Envia el mail con el codigo de recupero de la cuenta.
    public Response enviarMail(string email)
    {
        try
        {
            email = "\"" + email + "\"";
            string endpoint = "/WCS/api/perfil/email";
            Response response = Post(endpoint, email);
            return response;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Cambiar la contraseña una vez iniciada la sesion.
    public Response ingresarCodigoRecupero(string email, string code)
    {
        try
        {
            string endpoint = "/WCS/api/perfil/code";
            Response response = Post(endpoint, new { email, code });
            return response;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}