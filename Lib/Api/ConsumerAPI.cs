﻿namespace BaseFramework.Lib.Api
{
    public class ConsumerAPI
    {
        private HttpClient client;
        public ConsumerAPI()
        {
        }

        public async Task<List<Consumer>> GetApiResponseAsync(string accessToken = null)
        {
            try
            {
                string apiUrl = "https://192.168.10.111/QA/VC/DataApi/api/v1/consumer"; //host + "api/v1/auth/token";
                                                                                        // Configurar el cliente HTTP y deshabilitar la validación de certificados
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                HttpClient client = new HttpClient(handler);
                // Crear una solicitud GET al endpoint especificado
                var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);
                // Agregar un encabezado de autorización si se proporciona un token de acceso
                if (!string.IsNullOrEmpty(accessToken))
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                }
                // Realizar la solicitud HTTP GET
                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    // Deserializar la respuesta JSON en una lista de objetos ConsumerResponse
                    List<Consumer> consumers = JsonConvert.DeserializeObject<List<Consumer>>(responseContent);
                    return consumers;
                }
                else
                {
                    throw new HttpRequestException($"La solicitud no se completó con éxito. Código de estado: {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al realizar la solicitud: {ex.Message}");
            }
        }

        public async Task<Consumer> obtenerCodigoRecupero(string idUsuario, string accessToken = null)
        {
            try
            {
                string apiUrl = $"https://192.168.10.111/QA/VC/DataApi/api/v1/consumer/recovery?id={idUsuario}"; //host + "api/v1/consumer/recovery";

                // Configurar el cliente HTTP y deshabilitar la validación de certificados
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                HttpClient client = new HttpClient(handler);

                // Crear una solicitud GET al endpoint especificado
                var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

                // Agregar un encabezado de autorización si se proporciona un token de acceso
                if (!string.IsNullOrEmpty(accessToken))
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                }
                // Realizar la solicitud HTTP GET
                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();
                    // Deserializar la respuesta JSON en una lista de objetos ConsumerResponse
                    Consumer consumers = JsonConvert.DeserializeObject<Consumer>(responseContent);
                    return consumers;
                }
                else
                {
                    throw new HttpRequestException($"La solicitud no se completó con éxito. Código de estado: {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al realizar la solicitud: {ex.Message}");
            }
        }

        public async Task<List<Consumer>> obtenerTiendasNOpermitidas(string idUsuario, Boolean permitido, string accessToken = null)
        {
            try
            {
                string apiUrl = $"https://192.168.10.111/QA/VC/DataApi/api/v1/consumer/store/services?id={idUsuario}&allowed={permitido}";

                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => true;
                HttpClient client = new HttpClient(handler);

                var request = new HttpRequestMessage(HttpMethod.Get, apiUrl);

                if (!string.IsNullOrEmpty(accessToken))
                {
                    request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                }

                HttpResponseMessage response = await client.SendAsync(request);
                if (response.IsSuccessStatusCode)
                {
                    var responseContent = await response.Content.ReadAsStringAsync();

                    List<Consumer> consumers = JsonConvert.DeserializeObject<List<Consumer>>(responseContent);
                    return consumers;
                }
                else
                {
                    throw new HttpRequestException($"La solicitud no se completó con éxito. Código de estado: {response.StatusCode}");
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Error al realizar la solicitud: {ex.Message}");
            }
        }

    }
}
