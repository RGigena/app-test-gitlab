﻿using BaseFramework.Adapters;
using BaseFramework.Lib.Api.Models;
using BaseFramework.Lib.Api;
using Newtonsoft.Json;
using NUnit.Framework;

namespace BaseFramework.StepsDefinitions
{
    [Binding]
    public class CargarSaldoStep : Steps
    {
        private CreditClient? _creditClient;
        private Response _response;
        private readonly ScenarioContext _scenarioContext;
        private UserModel _userModel;
        private string _host;
        private EnvironmentManager _environment;
        private string _token;

        public CargarSaldoStep(ScenarioContext context)
        {
            _response = new Response();
            _scenarioContext = context;
            _userModel = new UserModel();
            _token = _scenarioContext.Get<string>("TOKEN");
            _host = _scenarioContext.Get<string>("host");
            _environment = _scenarioContext.Get<EnvironmentManager>("user");
            _creditClient = new CreditClient(_host, _token);
        }

        //CS.1 - INICIO
        [When(@"El usuario chequea el saldo disponible")]
        public void usuarioChequeaSaldo()
        {
            _response = _creditClient.consultarSaldo();

            if (_userModel.Status == "0")
            {
                if (!_scenarioContext.ContainsKey("SaldoResponse"))
                {
                    _scenarioContext.Add("SaldoResponse", _response);
                }
                else
                {
                    _scenarioContext["SaldoResponse"] = _response;
                }
            }
        }

        [Then(@"El usuario ve su saldo disponible")]
        public void saldoOk()
        {
            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("SaldoResponse").StatusCode, "Ocurrio un error al consultar saldo");
            Console.WriteLine("Saldo disponible: " + _userModel.Body?.Balance);
        }

        [When(@"El usuario agrega un monto a cargar")]
        public void montoACargar()
        {
            var monto = 200;
            _response = _creditClient!.cargarSaldo(monto);

            if (_userModel.Status == "0")
            {
                if (!_scenarioContext.ContainsKey("SaldoResponse"))
                {
                    _scenarioContext.Add("SaldoResponse", _response);
                }
                else
                {
                    _scenarioContext["SaldoResponse"] = _response;
                }
            }
        }

        [Then(@"Se obtiene la URL para finalizar pago")]
        public void urlPago()
        {
            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("SaldoResponse").StatusCode, "Ocurrio un error al obtener url");
            Console.WriteLine("URL PAGO: " + _userModel.Body?.Url);
        }
        //CS.1 - FIN
    }
}
