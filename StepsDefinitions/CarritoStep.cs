﻿using BaseFramework.Lib.Api;

namespace BaseFramework.StepsDefinitions
{
    [Binding]
    public class CarritoStep : Steps
    {
        private Response _response;
        private readonly ScenarioContext _scenarioContext;
        private CartModel _cartModel;
        private CartClient _cartClient;
        private string _host;
        private string _token;

        public CarritoStep(ScenarioContext context)
        {
            _response = new Response();
            _scenarioContext = context;
            _cartModel = new CartModel();
            _token = _scenarioContext.Get<string>("TOKEN");
            _host = _scenarioContext.Get<string>("host");
            _cartClient = new CartClient(_host, _token);
        }

        [When(@"El usuario vacia el carrito antes de continuar")]
        public void vaciarCarrito()
        {
            _response = _cartClient.Eliminar();

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }
        }

        [Then(@"Carrito Vacio")]
        public void carritoVacio()
        {
            Assert.AreEqual(200, _scenarioContext.Get<Response>("cartResponse").StatusCode, "Ocurrio un error al vaciar el carrito");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            Console.WriteLine("Carrito: " + _cartModel.message);
        }

        [When(@"El usuario quita las unidades del producto")]
        public void quitarProducto()
        {
            _response = _cartClient.EliminarItem(CompraStep.IdProducto);

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }

        }

        [Then(@"El producto fue retirado del carrito")]
        public void productoRetirado()
        {
            Assert.AreEqual(200, _scenarioContext.Get<Response>("cartResponse").StatusCode, "Ocurrio un error al quitar el producto");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            Console.WriteLine("Status: " + _cartModel.status + " Producto Retirado");
        }

        [When(@"El usuario vacia el carrito")]
        public void estadoCarrito()
        {
            _response = _cartClient.carritoActivo();

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }

            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            //RECORRE LA COLECCION PARA OBTENER LA DESCRIPCION DE CADA ITEM.
            //MUESTRA POR CONSOLA LA DESCRIPCION DE CADA UNO.
            foreach (var item in _cartModel.Body.items)
            {
                Console.WriteLine("Descripción: " + item.description);
            }
        }

        [When(@"El usuario controla el total a pagar")]
        public void totalAPagar()
        {
            _response = _cartClient.carritoActivo();

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }

            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            //RECORRE UNA COLECCION DE ITEMS
            //MULTIPLICA EL PRECIO DE CADA ITEM POR LA CANTIDAD
            //LUEGO SE LO SUMA AL TOTAL.
            decimal total = 0;
            foreach (var item in _cartModel.Body.items)
            {
                decimal subtotal = item.price * item.amount;
                total += subtotal;

                Console.WriteLine("Descripción: " + item.description);
                Console.WriteLine("Total del producto: " + subtotal);
            }
            Console.WriteLine("Total a abonar: " + total);
        }
    }
}
