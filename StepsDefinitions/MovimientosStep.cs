﻿namespace BaseFramework.StepsDefinitions
{
    [Binding]
    public class MovimientosStep : Steps
    {
        private TransactionsClient? _transactionsClient;
        private Response _response;
        private readonly ScenarioContext _scenarioContext;
        private UserModel _userModel;
        private CartModel _cartModel;
        private BodyModel _bodyModel;
        private string _host;
        private string _token;
        private EnvironmentManager _environment;
        private Random random = new Random();

        public MovimientosStep(ScenarioContext context)
        {
            _response = new Response();
            _scenarioContext = context;
            _userModel = new UserModel();
            _cartModel = new CartModel();
            _bodyModel = new BodyModel();
            _host = _scenarioContext.Get<string>("host");
            _token = _scenarioContext.Get<string>("TOKEN");
            _environment = _scenarioContext.Get<EnvironmentManager>("user");
            _transactionsClient = new TransactionsClient(_host, _token);
        }

        [When(@"El usuario ingresa a ver el listado de movimientos realizados")]
        public void obtenerMov()
        {
            _transactionsClient = new TransactionsClient(_host, _scenarioContext.Get<string>("TOKEN"));

            _response = _transactionsClient!.obtenerMovimientos();

            if (_userModel.Status == "0")
            {
                if (!_scenarioContext.ContainsKey("MovimientoResponse"))
                {
                    _scenarioContext.Add("MovimientoResponse", _response);
                }
                else
                {
                    _scenarioContext["MovimientoResponse"] = _response;
                }
            }
        }

        [Then(@"El usuario obtiene una lista con los movimientos realizados")]
        public void obtenerDetalle()
        {
            _bodyModel = JsonConvert.DeserializeObject<BodyModel>(_response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("MovimientoResponse").StatusCode, "Ocurrio un error al consultar movimientos");

            foreach (var item in _bodyModel.Body)
            {
                var description = item?["description"]?.ToString();
                if (!string.IsNullOrEmpty(description))
                {
                    Console.WriteLine("Descripción: " + description);
                }
            }
        }

        [When(@"El usuario selecciona un movimiento del listado para ver en detalle")]
        public void seleccionMovimientoDetalle()
        {
            _bodyModel = JsonConvert.DeserializeObject<BodyModel>(_response.Content)!;

            List<int> idVendingLista = new List<int>();

            foreach (var item in _bodyModel.Body)
            {
                var idVending = item?["idVendingConsumidorEstado"]?.ToObject<int>();
                if (idVending.HasValue)
                {
                    idVendingLista.Add(idVending.Value);
                }
            }

            int randomIndex = random.Next(0, idVendingLista.Count);
            int IdVending = idVendingLista[randomIndex];
            Console.WriteLine("Valor seleccionado al azar: " + IdVending);

            _response = _transactionsClient!.obtenerDetalleMovimiento(IdVending);

            if (_userModel.Status == "0")
            {
                if (!_scenarioContext.ContainsKey("MovimientoResponse"))
                {
                    _scenarioContext.Add("MovimientoResponse", _response);
                }
                else
                {
                    _scenarioContext["MovimientoResponse"] = _response;
                }
            }

        }

        [Then(@"Se muestra el movimiento en detalle")]
        public void detalleMovimiento()
        {
            _cartModel = JsonConvert.DeserializeObject<CartModel>(_response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("MovimientoResponse").StatusCode, "Ocurrio un error al consultar movimientos");

            Console.WriteLine("Descripcion: " + _cartModel.Body.Description);

        }
    }
}
