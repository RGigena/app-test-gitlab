﻿using BaseFramework.Adapters;
using BaseFramework.Lib.Api.Models;
using BaseFramework.Lib.Api;
using FluentAssertions;
using Newtonsoft.Json;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using System.Diagnostics.CodeAnalysis;

namespace BaseFramework.StepsDefinitions
{
    [Binding]
    internal class E2EStep: Steps
    {
        private LoginClient? _loginClient;
        private Response _response;
        private readonly ScenarioContext _scenarioContext;
        private UserModel _userModel;
        private TokenModel _tokenModel;

        public E2EStep(ScenarioContext context)
        {
            _response = new Response();
            _scenarioContext = context;
            _userModel = new UserModel();
            _tokenModel = new TokenModel();
        }

    }
}
