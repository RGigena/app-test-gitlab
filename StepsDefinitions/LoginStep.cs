[Binding]
public class LoginStep : Steps
{
    private PerfilClient _perfilClient;
    private SessionClient _sessionClient;
    private Response _response;
    private AuthorizationAPI _authorizationClient;
    private ConsumerAPI _consumerClient;
    private readonly ScenarioContext _scenarioContext;
    private UserModel _userModel;
    private EnvironmentManager _environment;
    private string _host;
    public static string? JwtToken { get; set; }
    public static string? HostTest { get; set; }

    public LoginStep(ScenarioContext context)
    {
        _response = new Response();
        _scenarioContext = context;
        _userModel = new UserModel();
        _authorizationClient = new AuthorizationAPI();
        _consumerClient = new ConsumerAPI();
        _host = _scenarioContext.Get<string>("host");
        _perfilClient = new PerfilClient(_host);
        _sessionClient = new SessionClient(_host);
        _environment = _scenarioContext.Get<EnvironmentManager>("user");
    }

    //LVC.1 - INICIO
    [Given(@"El usuario se loguea en la app")]
    public void usuarioSeLoguea()
    {
        //SE LE ASIGNA EL VALOR DE HOST A LA VARIABLE HostTest
        //PARA SER REUTILIZADA EN OTROS METODOS.
        HostTest = _host;

        _response = _sessionClient.Login(_environment.User.Nombre, _environment.User.Pass);

        //SE VERIFICA SI EL SCENARIOCONTEXT YA TIENE UNA CLAVE LLAMADA ''LOGINRESPONSE''
        //EN CASO DE NO TENERLA LA AGREGA, EN CASO DE TENERLA, SE ACTUALIZA EL VALOR.
        if (_userModel.Status == "0")
        {
            if (!_scenarioContext.ContainsKey("LoginResponse"))
            {
                _scenarioContext.Add("LoginResponse", _response);
            }
            else
            {
                _scenarioContext["LoginResponse"] = _response;
            }
        }
        else
        {
            //SE DESERIALIZA EL JSON PARA OBTENER EL MENSAJE.
            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

            //MUESTRE EL MENSAJE QUE DEVUELVE LA REQUEST.
            //EJ: ''INGRESE TODOS LOS CAMPOS''
            Console.WriteLine(_userModel.Message);
        }
    }

    [Then(@"Inicia sesion correctamente")]
    public void loginOk()
    {
        _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

        //JWT VARIABLE ESTATICA. 
        //LA DECLARO PARA REUTILIZARLA EN OTROS ESCENARIOS.
        JwtToken = _userModel.Body?.Token?.token;

        Assert.AreEqual(200, _scenarioContext.Get<Response>("LoginResponse").StatusCode, "Ocurrio un error al iniciar sesion");

        Assert.IsNotNull(_userModel.Body?.Token.token, "Token NULO");

        Assert.IsNotEmpty(_userModel.Body?.Token.token, "Token VACIO");

        Console.WriteLine("Token JWT: " + _userModel.Body?.Token?.token);

        //CONTROL DE CLAVES
        if (_scenarioContext.ContainsKey("TOKEN"))
        {
            _scenarioContext["TOKEN"] = JwtToken;
        }
        else
        {
            _scenarioContext.Add("TOKEN", JwtToken);
        }
    }
    //LVC.1 - FIN

    //LVC.2 - INICIO
    [When(@"El usuario se loguea en la app")]
    public void elusuarioIniciaSesion()
    {
        //HAGO REFERENCIA AL GIVEN PARA NO REPETIR STEPS NI CODIGO.
        Given("El usuario se loguea en la app");
    }

    [When(@"El usuario cambia la contrasenia")]
    public void cambioContrasenia()
    {
        _response = _perfilClient.cambiarContrasenia(_environment.User.Mail, _environment.User.NewPass);
        _scenarioContext.Add("newPassResponse", _response);
    }

    [Then(@"La contrasenia se actualiza correctamente")]
    public void loginConNuevaContrasenia()
    {
        _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

        Assert.AreEqual(200, _scenarioContext.Get<Response>("newPassResponse").StatusCode, "Ocurrio un error al cambiar la contraseņa");

        Assert.AreEqual(_userModel.Message, "La contraseņa ha sido actualizada correctamente");

        Console.WriteLine("Mensaje: " + _userModel.Message);
    }
    //LVC.2 - FIN

    //LVC.3 - INICIO
    [Given(@"El usuario olvido su contrasenia")]
    public void usuarioOlvidaContrasenia()
    {
        _response = _sessionClient.Login(_environment.User.Nombre, _environment.User.PassErroneo);

        _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

        if (_userModel.Status == "0")
        {
            if (!_scenarioContext.ContainsKey("LoginResponse"))
            {
                _scenarioContext.Add("LoginResponse", _response);
            }
            else
            {
                _scenarioContext["LoginResponse"] = _response;
            }
        }
        else
        {
            Console.WriteLine(_userModel.Message);
        }
    }

    [When(@"El usuario recupera su contrasenia")]
    public void usuarioRecuperaContrasenia()
    {
        _response = _perfilClient.enviarMail(_environment.User.Mail);

        _scenarioContext.Add("mailResponse", _response);

        _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

        Assert.AreEqual(200, _scenarioContext.Get<Response>("mailResponse").StatusCode, "Ocurrio un error al recuperar la contraseņa");

        Assert.AreEqual(_userModel.Message, "Email enviado correctamente. Revise su bandeja de entrada");

        Console.WriteLine("Mensaje: " + _userModel.Message);
    }

    [StepDefinition(@"El usuario recibe el codigo de recupero")]
    public async Task usuarioRecibeCodigo()
    {
        var environment = _scenarioContext.Get<EnvironmentManager>("user");

        string token = await _authorizationClient.GetAccessTokenAsync(_environment.User.userToken, _environment.User.passToken);

        Consumer respuesta = await _consumerClient.obtenerCodigoRecupero(_environment.User.idUsuario, token);

        Console.WriteLine("Codigo de recupero: " + respuesta.RecoveryCode);

        _scenarioContext.Add("consumerResponse", respuesta);
    }

    [StepDefinition(@"El usuario valida codigo de recupero")]
    public void validaCodigoRecupero()
    {
        var respuestaConsumer = _scenarioContext.Get<Consumer>("consumerResponse");

        _response = _perfilClient.ingresarCodigoRecupero(_environment.User.Mail, respuestaConsumer.RecoveryCode);

        _scenarioContext.Add("codigoResponse", _response);

        _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

        Assert.AreEqual(200, _scenarioContext.Get<Response>("codigoResponse").StatusCode, "Ocurrio un error al comprobar el codigo");

        Assert.AreEqual(_userModel.Message, "Ingrese su nueva contraseņa");

        Console.WriteLine("Mensaje: " + _userModel.Message);
    }

    [Then(@"El usuario restablece la contrasenia")]
    public void restableceContrasenia()
    {
        _response = _perfilClient.cambiarContrasenia(_environment.User.Mail, _environment.User.NewPass);

        _scenarioContext.Add("newPassResponse", _response);

        _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

        Assert.AreEqual(200, _scenarioContext.Get<Response>("newPassResponse").StatusCode, "Ocurrio un error al cambiar la contraseņa");

        Assert.AreEqual(_userModel.Message, "La contraseņa ha sido actualizada correctamente");

        Console.WriteLine("Mensaje: " + _userModel.Message);
    }
    //LVC.3 - FIN

}