﻿using BaseFramework.Lib.Api;

namespace BaseFramework.StepsDefinitions
{
    [Binding]
    internal class ExceptionStep : Steps
    {
        private Response _response;
        private SessionClient? _sessionClient;
        private PerfilClient? _perfilClient;
        private readonly ScenarioContext _scenarioContext;
        private UserModel _userModel;
        private CartClient? _cartClient;
        private CartModel _cartModel;
        private AuthorizationAPI _authorizationClient;
        private ConsumerAPI _consumerClient;
        private EnvironmentManager _environment;
        private string _host;

        public ExceptionStep(ScenarioContext context)
        {
            _response = new Response();
            _scenarioContext = context;
            _userModel = new UserModel();
            _authorizationClient = new AuthorizationAPI();
            _consumerClient = new ConsumerAPI();
            _host = _scenarioContext.Get<string>("host");
            _sessionClient = new SessionClient(_host);
            _environment = _scenarioContext.Get<EnvironmentManager>("user");
        }

        //MOCK

        //@E.1 -- INICIO --
        [Given(@"El usuario no tiene permisos de consumo para el producto A")]
        public async Task quitarPermisoProducto()
        {
            //TODO: Este no va, es consumo en tiendas no de productos.
            string token = await _authorizationClient.GetAccessTokenAsync(_environment.User.userToken, _environment.User.passToken);

            List<Consumer> listaTiendas = await _consumerClient.obtenerTiendasNOpermitidas(_environment.User.idUsuario, false, token);

            _scenarioContext.Add("listaTiendasResponse", listaTiendas);
        }

        [When(@"Se escanea el QR del producto A")]
        public void escaneaProductoA()
        {
            _cartClient = new CartClient(_host, _scenarioContext.Get<string>("TOKEN"));

            string qr = "https://cg-qr.com?rR09zVgb7Rjaer_vqUqc-w==";
            _response = _cartClient.obtenerQr(qr);

            if (!_scenarioContext.ContainsKey("productoResponse"))
            {
                _scenarioContext.Add("productoResponse", _response);
            }
            else
            {
                _scenarioContext["productoResponse"] = _response;
            }

            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;
            
            Console.WriteLine("Producto seleccionado: " + _userModel.Body.id);

            var response = _cartClient.obtenerProducto(_userModel.Body.id).Result;

            _scenarioContext.Add("productoAResponse", response);
        }

        [Then(@"El usuario no tiene permitido consumir el producto A")]
        public void consumoNoPermitido()
        {
            var response = _scenarioContext.Get<Response>("productoAResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("productoAResponse").StatusCode, "Ocurrio un error al obtener");

            Assert.AreEqual(_cartModel.message, "NO tienes permisos para adquirir este producto");

            Console.WriteLine(_cartModel.message);
        }
        //@E.1 -- FIN --

        //@E.2
        [Given(@"El usuario No tiene permisos de consumo en la tienda A")]
        public void permisosTiendaA()
        {
            var idConsumidor = 1;
            var idTienda = 1;

            _cartClient = new CartClient(_host);

            var response = _cartClient.quitarPermisosTienda(idConsumidor, idTienda).Result;
            _scenarioContext.Add("quitarPermisosResponse", response);

            var respuesta = _scenarioContext.Get<Response>("quitarPermisosResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(respuesta.Content)!;

            Assert.AreEqual(_cartModel.message, "Permisos de consumo en la tienda actualizados");

            Console.WriteLine(_cartModel.message);
        }

        [When(@"Se escanea el QR del producto A de la tienda A")]
        public void escaneaProductoATiendaA()
        {
            When("Se escanea el QR del producto A");
        }

        [Then(@"El usuario no puede consumir en la tienda A")]
        public void tiendaANoPermitida()
        {
            var response = _scenarioContext.Get<Response>("productoAResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("productoAResponse").StatusCode, "Ocurrio un error al obtener");

            Assert.AreEqual(_cartModel.message, "NO tienes permisos para adquirir este producto");

            Console.WriteLine(_cartModel.message + " en esta tienda.");
        }
        //@E.2 -- FIN --

        //@E.3 -- INICIO --
        [Given(@"Se limita al usuario la carga electronica de credito")]
        public void cargaCreditoLimitada()
        {
            var idConsumidor = 1;

            _cartClient = new CartClient(_host);

            var response = _cartClient.quitarCargaElectronica(idConsumidor).Result;
            _scenarioContext.Add("quitarTiendasResponse", response);

            var respuesta = _scenarioContext.Get<Response>("quitarTiendasResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(respuesta.Content)!;

            Assert.AreEqual(_cartModel.message, "Permisos de carga actualizados");

            Console.WriteLine(_cartModel.message);
        }

        [When(@"El usuario procede a realizar una carga de saldo")]
        public void cargarSaldo()
        {
            _cartClient = new CartClient(_host, _scenarioContext.Get<string>("TOKEN"));

            var response = _cartClient?.cargaCredito().Result;

            _scenarioContext.Add("creditoResponse", response);
        }

        [Then(@"El usuario no esta habilitado para la carga electronica")]
        public void mensajeCredito()
        {
            var response = _scenarioContext.Get<Response>("creditoResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("creditoResponse").StatusCode, "Ocurrio un error al obtener credito");

            Assert.AreEqual(_cartModel.message, "No está habilitado para la carga de crédito electrónica.");

            Console.WriteLine(_cartModel.message);
        }
        //@E.3 -- FIN --

        //@E.4 -- INICIO --
        [Given(@"Se le quita al usuario el saldo")]
        public void quitaDeSaldo()
        {
            var idConsumidor = 1;
            var monto = 0;

            _cartClient = new CartClient(_host);

            var response = _cartClient.quitarSaldo(idConsumidor, monto).Result;
            _scenarioContext.Add("quitarSaldoResponse", response);

            var respuesta = _scenarioContext.Get<Response>("quitarSaldoResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(respuesta.Content)!;

            Assert.AreEqual(_cartModel.message, "Saldo Actualizado");

            Console.WriteLine(_cartModel.message);
        }

        [Then(@"El usuario no tiene saldo para realizar la compra")]
        public void usuarioSinSaldo()
        {

            _response = _cartClient.compraFallida().Result;

            if (!_scenarioContext.ContainsKey("cartResponse"))
            {
                _scenarioContext.Add("cartResponse", _response);
            }
            else
            {
                _scenarioContext["cartResponse"] = _response;
            }

            var response = _scenarioContext.Get<Response>("cartResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("cartResponse").StatusCode, "Ocurrio un error al finalizar compra");

            Assert.AreEqual(_cartModel.message, "No posee el saldo suficiente para realizar esta compra");

            Console.WriteLine(_cartModel.message);
        }
        //@E.4 -- FIN --

        //@E.5 -- INICIO --
        [Given(@"Se limita la cantidad de consumo del producto A al usuario A")]
        public void limiteConsumoProductoA()
        {
            var idConsumidor = 1;
            var idProducto = 1;
            var cantidadUnidades = 1;

            _cartClient = new CartClient(_host);

            var response = _cartClient.limitarProducto(idConsumidor, idProducto, cantidadUnidades).Result;
            _scenarioContext.Add("productoResponse", response);

            var respuesta = _scenarioContext.Get<Response>("productoResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(respuesta.Content)!;

            Assert.AreEqual(_cartModel.message, "Se limitó la cantidad de unidades a consumir");

            Console.WriteLine(_cartModel.message);
        }

        [When(@"Se escanea el QR del producto limitado A")]
        public void escaneaProductoLimitadoA()
        {
            When("Se escanea el QR del producto A");
        }

        [StepDefinition(@"El usuario agrega al carrito una cantidad mayor a la permitida del producto A")]
        public void agregaProductoLimitadoA()
        {
            _cartClient = new CartClient(_host, _scenarioContext.Get<string>("TOKEN"));

            var unidades = 2;

            _response = _cartClient.agregarProductoLimitadoA(unidades).Result;

            if (!_scenarioContext.ContainsKey("agregarPLAResponse"))
            {
                _scenarioContext.Add("agregarPLAResponse", _response);
            }
            else
            {
                _scenarioContext["agregarPLAResponse"] = _response;
            }
        }

        [Then(@"El usuario supero el limite del producto A")]
        public void consumoSuperado()
        {
            var response = _scenarioContext.Get<Response>("agregarPLAResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("agregarPLAResponse").StatusCode, "Ocurrio un error al agregar producto");

            Assert.AreEqual(_cartModel.message, "EL usuario no puede consumir esa cantidad de unidades del producto");

            Console.WriteLine(_cartModel.message);
        }
        //@E.5 -- FIN --

        //@E.6 -- INICIO --
        [When(@"se escanea el QR del producto A de la tienda A")]
        public void escaneaQRProductoTiendaA()
        {
            When("Se escanea el QR del producto seleccionado");
        }

        [Then(@"se agrega el producto A de la tienda A")]
        public void agregaProductoTiendaA()
        {
            When("Se agrega producto al carrito");
        }

        [When(@"se escanea el QR del producto B de la tienda B")]
        public void escaneaQRProductoTiendaB()
        {
            When("Se escanea el QR del producto seleccionado");
        }

        [Then(@"el usuario no puede consumir en dos tiendas a la vez")]
        public void usuarioNoPuedeComprar()
        {
            _cartClient = new CartClient(_host, _scenarioContext.Get<string>("TOKEN"));

            _response = _cartClient.usuarioNoPuedeComprar().Result;

            if (!_scenarioContext.ContainsKey("agregarPLAResponse"))
            {
                _scenarioContext.Add("agregarPLAResponse", _response);
            }
            else
            {
                _scenarioContext["agregarPLAResponse"] = _response;
            }

            var response = _scenarioContext.Get<Response>("agregarPLAResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("agregarPLAResponse").StatusCode, "Ocurrio un error al agregar producto");

            Assert.AreEqual(_cartModel.message, "El usuario no puede comprar en dos tiendas distintas a la vez.");

            Console.WriteLine(_cartModel.message);
        }
        //@E.6 -- FIN --

        //@E.7 -- INICIO --
        [When(@"El usuario procede a finalizar la compra nuevamente")]
        public void compraDuplicada()
        {
        }

        [Then(@"El carrito no existe")]
        public void carritoNoExiste()
        {
            try
            {
                _cartClient = new CartClient(_host, _scenarioContext.Get<string>("TOKEN"));

                _response = _cartClient.pagarProducto();
            }
            catch (HttpRequestException ex)
            {
                Assert.AreEqual(ex.Message, "Request failed with status code BadRequest", "Ocurrió un error al proceder con el pago del carrito. Por favor contáctese con soporte: {ex.Message}");
            }
        }
        //@E.7 -- FIN --

        //@E.8 -- INICIO --
        [When(@"El usuario procede a vaciar el carrito")]
        public void vaciarCarritoPostCompra()
        {
            When("El usuario vacia el carrito");
        }

        [Then(@"El usuario no puede vaciar el carrito")]
        public void carritoVacioError()
        {
            Then("Carrito Vacio");
        }
        //@E.8 -- FIN --

        //@E.9 -- INICIO --
        [Given(@"El usuario se loguea en la app con user name incorrecto")]
        public void usuarioIngresaMalElNombre()
        {
            _response = _sessionClient.Login(_environment.User.UserInc, _environment.User.Pass);

            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

            if (_userModel.Status == "0")
            {
                if (!_scenarioContext.ContainsKey("LoginResponse"))
                {
                    _scenarioContext.Add("LoginResponse", _response);
                }
                else
                {
                    _scenarioContext["LoginResponse"] = _response;
                }
            }
            else
            {
                Console.WriteLine(_userModel.Message);
            }
        }

        [Then(@"El usuario corrige el usuario")]
        public void corrigeUsuario()
        {
            Given("El usuario se loguea en la app");
        }
        //@E.9 -- FIN --

        //@E.10 -- INICIO --
        [Given(@"El usuario se loguea en la app con pass incorrecta")]
        public void usuarioOlvidaContrasenia()
        {
            _response = _sessionClient.Login(_environment.User.Nombre, _environment.User.PassErroneo);

            _userModel = JsonConvert.DeserializeObject<UserModel>(_response.Content)!;

            if (_userModel.Status == "0")
            {
                if (!_scenarioContext.ContainsKey("LoginResponse"))
                {
                    _scenarioContext.Add("LoginResponse", _response);
                }
                else
                {
                    _scenarioContext["LoginResponse"] = _response;
                }
            }
            else
            {
                Console.WriteLine(_userModel.Message);
            }
        }

        [Then(@"El usuario corrige la contrasenia")]
        public void corrigePass()
        {
            Given("El usuario se loguea en la app");
        }
        //@E.10 -- FIN --

        //@E.11 -- INICIO --
        [Given(@"Se suspende la cuenta del usuario")]
        public void seSuspendeCuenta()
        {
            var idConsumidor = 1;

            _cartClient = new CartClient(_host);

            var response = _cartClient.suspenderUsuario(idConsumidor).Result;
            _scenarioContext.Add("suspendidoResponse", response);
        }

        [Then(@"El usuario recibe el mensaje de cuenta suspendida")]
        public void cuentaSuspendida()
        {
            var response = _scenarioContext.Get<Response>("suspendidoResponse");

            _cartModel = JsonConvert.DeserializeObject<CartModel>(response.Content)!;

            Assert.AreEqual(200, _scenarioContext.Get<Response>("suspendidoResponse").StatusCode, "Ocurrio un error al agregar producto");

            Assert.AreEqual(_cartModel.message, "Esta cuenta esta suspendida");

            Console.WriteLine(_cartModel.message);
        }
        //@E.11 -- FIN --

        //@E.12 -- INICIO --
        [When(@"Se suspende la cuenta del usuario")]
        public void suspendeCuentaUsuario()
        {
            Given("Se suspende la cuenta del usuario");
        }

        //@E.12 -- FIN --
    }
}
